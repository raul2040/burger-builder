import React from "react";

import classes from "./Burger.css";
import BurgerIngredient from "./BurgerIngredient/BurgerIngredient";

const burger = props => {
  let transformedIngredients = Object.keys(props.ingredients).map(igKey => {
    console.log(igKey);
    return [...Array(props.ingredients[igKey])].map((_, i) => {
      return <BurgerIngredient key={igKey + i} type={igKey} />;
    } );
  } )
  .reduce((arr, el) => {
    return arr.concat(el)
  }, []);
if (transformedIngredients.length === 0){
    transformedIngredients = <p> Please start adding ingredients!</p>;
}

  console.log(transformedIngredients);
  // la clase Object de JS tiene un método que extrae las claves de un objeto dado y las convierte en una matriz
  // Iteramos con (igKey) y vamos guardando los elementos en una array, de longitud de props ingredients
  // luego iteramos sobre el array creado
  return (
    <div className={classes.Burger}>
      {/* Le añadimos el estilo */}
      <BurgerIngredient type="bread-top" />
      {transformedIngredients}
      <BurgerIngredient type="bread-bottom" />
      {/* Le añadimos los ingredientes a nuesta hamburguesa, usando el component BurgerIngredient, le pasamos como props el tipo de ingrediente */}
    </div>
  );
};

export default burger;

