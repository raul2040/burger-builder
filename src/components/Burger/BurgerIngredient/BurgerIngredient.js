import React,{ Component } from "react";

import classes from "./BurgerIngredient.css";
import PropsTypes from 'prop-types';

class BurgerIngredient extends Component {
    render() {
    let ingredient = null;

  //Hacemos un switch con el tipo de ingrediente que nos vayan a pasar
  switch ( this.props.type ) {
    case "bread-bottom":
      ingredient = <div className={classes.BreadBottom} />;
      break;
    case "bread-top":
      ingredient = (
        <div className={classes.BreadTop}>
          <div className={classes.Seeds1} />
          <div className={classes.Seeds2} />
        </div>
      );
      break;
    case "meat":
      ingredient = <div className={classes.Meat}/>;
      break;
    case "cheese":
      ingredient = <div className={classes.Cheese}/>;
      break;
    case "salad":
      ingredient = <div className={classes.Salad}/>;
      break;
    case "bacon":
      ingredient = <div className={classes.Bacon}/>;
      break;
    default:
      ingredient = null;
    }
    
    return ingredient;
    
    }   
}
BurgerIngredient.propTypes = {
    type: PropsTypes.string.isRequired
};

//Le decimos que la props que le pasamos tiene que ser una string y que es obligatorio.


export default BurgerIngredient;
