import React from "react";
import Aux from "../../hoc/Aux";
import classes from './Layout.css';

//Creamos la función layout que devuelve un elemento react creado con jsx
//El equipo de React ha declarado que este tipo de componentes es la manera apropiada de crear
//componentes sin estado y que en un futuro se le dará mayor atención y mejoras a los mismos.

const layout = props => (
  <Aux>
    <div>Toolbar, SideDrawer, Backdrop</div>
    <main className={classes.Content}>
        {props.children}
    </main>
  </Aux>
  //Envolvemos todo con el componente HOC/Auxiliar
);

export default layout;
