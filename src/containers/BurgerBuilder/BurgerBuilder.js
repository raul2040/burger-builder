import React, { Component } from 'react';
//Manera de hacer los import para una (clase) en react.
import Aux from '../../hoc/Aux';
import Burger from '../../components/Burger/Burger';

class BurgerBuilder extends Component {
    // constructor(props){
    //     super(props);
    //     this.state = {...}
    // }
    state = {
        ingredients: {
            salad:0,
            bacon:0,
            cheese:0,
            meat:0
        }
    }

    render() {
    // Debemos devolver un elemento react utilizando JSX
        return (
            <Aux>
            <div>
                <Burger ingredients={this.state.ingredients} />
                <div>Build Controls</div>
            </div>
            </Aux>
        );
    }
}

export default BurgerBuilder;